//
//  main.m
//  ArrayFun
//
//  Created by Bobby Skinner on 6/21/15.
//  Copyright (c) 2015 Bobby Skinner. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
