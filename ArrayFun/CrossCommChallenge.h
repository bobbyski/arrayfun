//
//  CrossCommChallenge.h
//  ArrayFun
//
//  Created by Bobby Skinner on 6/21/15.
//  Copyright (c) 2015 Bobby Skinner. All rights reserved.
//

#import <Foundation/Foundation.h>

#define CCC_ROW_SPAN                0
#define CCC_COL_SPAN                1

#define CCC_NULL                    [NSNull null]

/**
 *  This class is the programming challenge from CrossComm.
 *
 *  It is intended to demonstrate overall coding style. It
 *  is somewhat over engineered so that it can demonstrate
 *  a little more of my coding style.
 *
 *  The purpose of this class is to transform an array of
 *  source data into an array representing the data with 
 *  "null" marking the duplicated colums
 *
 *  @note This class is not thread safe
 */
@interface CrossCommChallenge : NSObject

/**
 *  The source data for transformation. This should be
 *  array may contain an array of arrays representing the
 *  data containing NSString data.
 *
 *  The array should be in the form:
 *
 *    [["1","2"],["3","4","5"]]
 */
@property (nonatomic, strong) NSArray* sourceData;

/**
 *  The span data property contains row and column splanning
 *  charicteristics. This is a dictionary containing the 
 *  data and the row spaning information.
 *
 *  The dictionary should have the form:
 *
 *    [{"1":[2,1]},{"2":[1,3]}]
 */
@property (nonatomic, strong) NSDictionary* spanData;

/**
 *  The result data property will contain the resulting array
 *  having null indicating the cells that are spanned. so 
 *  given the examples above, the result should be:
 *
 *    [[1,2,null,null],[null, 3,4,5]]
 */
@property (nonatomic, readonly) NSArray* resultData;

/**
 *  Perform a transformation of the data. This is basically 
 *  a convienance method to execute a transformation without 
 *  instanciating your own copy of the class
 *
 *  @param data  data to trnasfor
 *  @param spans thspan data
 *
 *  @return the transformed array
 */
+ (NSArray*)transform:(NSArray*)data withSpans:(NSDictionary*)spans;

@end
