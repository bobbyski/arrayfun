//
//  CrossCommChallenge.m
//  ArrayFun
//
//  Created by Bobby Skinner on 6/21/15.
//  Copyright (c) 2015 Bobby Skinner. All rights reserved.
//

#import "CrossCommChallenge.h"

@interface CrossCommChallenge ()

// this is the working storage for the transformed data
@property (nonatomic, strong) NSMutableArray* transformedData;

// the internal data is invalid an needs to be reset
//
// NOTE: this flag is not intended to be thread safe, so
//       it is possible for an update to be missed if the
//       inputs are changed while the output is retrieved
//
//       this is adequate for this demonstration but more
//       care may be warrented in a real world
//       implementation
@property (nonatomic, assign) BOOL dirty;

@end

@implementation CrossCommChallenge

+ (NSArray*)transform:(NSArray*)data withSpans:(NSDictionary*)spans
{
    CrossCommChallenge* transformer = [CrossCommChallenge new];
    
    [transformer setSpanData: spans];
    [transformer setSourceData: data];
    
    return [transformer resultData];
}

#pragma mark - internal methods -

/**
 *  @internal
 *
 *  This method will perform the transformation of the
 *  data.
 *
 *  It repetativly calls addDate:forRow to insert the 
 *  data into the results array
 */
- (void) transformData
{
    // create a clean starting point
    self.transformedData = [NSMutableArray array];
    NSInteger row = 0;
    
    // loop through each row
    for( NSArray* rowData in self.sourceData )
    {
        NSInteger count = 0;
        
        // add all the elements for the row
        for( id <NSCopying> item in rowData )
            if ( [self addData: item forRow: row] )
                count++;
        
        // increment the row if we added any (This accounts for spans of 0)
        if ( count )
            row++;
    }
    
    // fill to the end with nulls to match spec
    [self fiilWithNullsStartingAt: 0 for: [self.transformedData count]];
    
    self.dirty = NO;
}

/**
 *  @internal
 *
 *  Add the data to the array and account for callumn 
 *  and row spans
 *
 *  @param data the data to load
 *  @param row  row the data sould be added to
 */
- (BOOL) addData:(id <NSCopying>)data forRow:(NSInteger)row
{
    BOOL result = NO;
    
    // get span data from the associated dictionary
    NSArray* spanData = self.spanData[data];
    NSInteger rowCount = 1;
    NSInteger colCount = 1;

    if ( [spanData count] > CCC_ROW_SPAN )
        rowCount = [spanData[CCC_ROW_SPAN] floatValue];
    
    if ( [spanData count] > CCC_COL_SPAN )
        colCount = [spanData[CCC_COL_SPAN] floatValue];
    
    if ( ( rowCount > 0 ) && ( colCount > 0 ) )
    {
        result = YES;
        
        [self fiilWithNullsStartingAt: row for: rowCount];
        
        for( NSInteger rowNum=0 ; rowNum<rowCount ; rowNum++ )
        {
            NSMutableArray* current = self.transformedData[row + rowNum];
            
            for( NSInteger colNum=0 ; colNum<colCount ; colNum++ )
            {
                if ( ( colNum == 0 ) && ( rowNum == 0 ) )
                    [current addObject: data];
                else
                    [current addObject: CCC_NULL];
            }
        }
    }
    
    return result;
}

/**
 *  @internal
 *
 *  Fill the rows that will be spanned to the same starting 
 *  width
 *
 *  @param start starting row
 *  @param count number of rows
 */
- (void)fiilWithNullsStartingAt:(NSInteger)start for:(NSInteger)count
{
    NSInteger max = 0;
    
    // see which is the longest
    for( NSInteger i=0 ; i<count ; i++ )
    {
        NSMutableArray* row = nil;
        
        if ( [self.transformedData count] > start + i )
            row = self.transformedData[start + i];
        else
        {
            // add the row if it isn't there
            row = [NSMutableArray array];
            [self.transformedData addObject: row];
        }
        
        if ( [row count] > max )
            max = [row count];
    }
    
    // even out the rows
    for( NSInteger i=0 ; i<count ; i++ )
    {
        NSMutableArray* row = self.transformedData[start + i];
        
        while( [row count] < max )
        {
            [row addObject: CCC_NULL];
        }
    }
}

#pragma mark - property setters and getters -

- (void) setSourceData:(NSArray *)sourceData
{
    _sourceData = sourceData;
    
    // mark as dirty so the data will be rebuilt on next access
    self.dirty = YES;
}

- (void) setSpanData:(NSDictionary *)spanData
{
    _spanData = spanData;
    
    // mark as dirty so the data will be rebuilt on next access
    self.dirty = YES;
}

- (NSArray*) resultData
{
    if ( self.dirty )
        [self transformData];
    
    // return a shallow copy, so that any UI copies may be
    // referenced safely.
    //
    // NOTE: Not thread safe, but should be adequate if
    //       called from the main thread
    return [NSArray arrayWithArray: self.transformedData];
}

@end
