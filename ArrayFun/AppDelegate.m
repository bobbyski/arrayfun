//
//  AppDelegate.m
//  ArrayFun
//
//  Created by Bobby Skinner on 6/21/15.
//  Copyright (c) 2015 Bobby Skinner. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

@end
