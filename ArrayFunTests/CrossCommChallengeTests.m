//
//  CrossCommChallengeTests.m
//  ArrayFun
//
//  Created by Bobby Skinner on 6/21/15.
//  Copyright (c) 2015 Bobby Skinner. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <XCTest/XCTest.h>
#import "CrossCommChallenge.h"

@interface CrossCommChallengeTests : XCTestCase

@end

@implementation CrossCommChallengeTests

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

// This is the CrossComm supplied case
- (void)testProvidedSample
{
    NSArray* result = [CrossCommChallenge transform: @[
                                                       @[@"1",@"2"],
                                                       @[@"3",@"4",@"5"]
                                                       ]
                                          withSpans: @{
                                                       @"1": @[@2,@1],
                                                       @"2": @[@1, @3]
                                                       }];
    
    NSArray* expectations = @[
                              @[@"1", @"2", CCC_NULL, CCC_NULL],
                              @[CCC_NULL, @"3", @"4", @"5"]
                              ];
    
    XCTAssert( [result isEqual: expectations],
              "Result:\n%@\nExpected:%@", result, expectations );

}

// The simpleest of test cases
- (void)testSimple
{
    NSArray* result = [CrossCommChallenge transform: @[
                                                       @[@"1",@"2"],
                                                       @[@"3",@"4",@"5"]
                                                       ]
                                          withSpans: nil];
    
    NSArray* expectations = @[
                              @[@"1", @"2", CCC_NULL],
                              @[@"3", @"4", @"5"]
                              ];
    
    XCTAssert( [result isEqual: expectations],
              "Result:\n%@\nExpected:%@", result, expectations );
    
}

- (void)testMoreComplex
{
    NSArray* result = [CrossCommChallenge transform: @[
                                                       @[@"1",@"2"],
                                                       @[@"3",@"1",@"5"]
                                                       ]
                                          withSpans: @{
                                                       @"1": @[@2,@1],
                                                       @"2": @[@1, @3]
                                                       }];
    
    NSArray* expectations = @[
                              @[@"1", @"2", CCC_NULL, CCC_NULL],
                              @[CCC_NULL, @"3", @"1", @"5"],
                              @[CCC_NULL, CCC_NULL, CCC_NULL, CCC_NULL]
                              ];
    
    XCTAssert( [result isEqual: expectations],
              "Result:\n%@\nExpected:%@", result, expectations );
    
}

// We specify strings but any <NSCopying> objects should work
- (void)testNumbers
{
    NSArray* result = [CrossCommChallenge transform: @[
                                                       @[@1,@2],
                                                       @[@3,@1,@5]
                                                       ]
                                          withSpans: @{
                                                       @1: @[@2,@1],
                                                       @2: @[@1, @3]
                                                       }];
    
    NSArray* expectations = @[
                              @[@1, @2, CCC_NULL, CCC_NULL],
                              @[CCC_NULL, @3, @1, @5],
                              @[CCC_NULL, CCC_NULL, CCC_NULL, CCC_NULL]
                              ];
    
    XCTAssert( [result isEqual: expectations],
              "Result:\n%@\nExpected:%@", result, expectations );
    
}

// This example is complex enough to question the spec, I could ciome up
// with 2 possible solutions. I chose to stick with the simpler since this
// is not a real project
- (void)testEvenMoreComplex
{
    NSArray* result = [CrossCommChallenge transform: @[
                                                       @[@"1",@"2"],
                                                       @[@"3",@"1",@"5"],
                                                       @[@"4", @"5"]
                                                       ]
                                          withSpans: @{
                                                       @"1": @[@2,@1],
                                                       @"2": @[@1, @3]
                                                       }];
    
    NSArray* expectations = @[
                              @[@"1", @"2", CCC_NULL, CCC_NULL, CCC_NULL],
                              @[CCC_NULL, @"3", @"1", @"5", CCC_NULL],
                              @[CCC_NULL, CCC_NULL, CCC_NULL, @"4", @"5"]
                              ];
    
// NOTE: They may have intended this but it complicates the algolrithm,
//       so I am going with the simpler interpretation
//    NSArray* expectations = @[
//                              @[@"1", @"2", CCC_NULL, CCC_NULL],
//                              @[CCC_NULL, @"3", @"1", @"5"],
//                              @[@"4", CCC_NULL, @"5", CCC_NULL]
//                              ];
    
    XCTAssert( [result isEqual: expectations],
              "Result:\n%@\nExpected:%@", result, expectations );
    
}

// test a case where both row and span are specified
- (void)testTwoTwo
{
    NSArray* result = [CrossCommChallenge transform: @[
                                                       @[@"1",@"2"],
                                                       @[@"4",@"1",@"5"],
                                                       @[@"3", @"5"]
                                                       ]
                                          withSpans: @{
                                                       @"1": @[@2,@1],
                                                       @"2": @[@1, @3],
                                                       @"4": @[@2, @2]
                                                       }];
    
    NSArray* expectations = @[
                              @[@"1",     @"2",     CCC_NULL, CCC_NULL, CCC_NULL, CCC_NULL],
                              @[CCC_NULL, @"4",     CCC_NULL, @"1",     @"5",     CCC_NULL],
                              @[CCC_NULL, CCC_NULL, CCC_NULL, CCC_NULL, @"3",     @"5"]
                              ];
    
    // NOTE: This one would also need to change based on the above assumptions
    
    XCTAssert( [result isEqual: expectations],
              "Result:\n%@\nExpected:%@", result, expectations );
    
}

// OK, this one is just being silly
- (void)testCraziness
{
    NSArray* result = [CrossCommChallenge transform: @[
                                                       @[@"1"],
                                                       @[@"2", @"1"],
                                                       @[@"3", @"2", @"1"],
                                                       @[@"4", @"3", @"2", @"1"]
                                                       ]
                                          withSpans: @{
                                                       @"1": @[@2,@1],
                                                       @"2": @[@1, @2],
                                                       @"3": @[@2, @2],
                                                       @"4": @[@1, @3]
                                                       }];
    
    NSArray* expectations = @[
                              @[@"1",     CCC_NULL, CCC_NULL, CCC_NULL, CCC_NULL, CCC_NULL,
                                          CCC_NULL, CCC_NULL, CCC_NULL, CCC_NULL, CCC_NULL,
                                          CCC_NULL, CCC_NULL, CCC_NULL, CCC_NULL, CCC_NULL,
                                          CCC_NULL],
                              @[CCC_NULL, @"2",     CCC_NULL, @"1",     CCC_NULL, CCC_NULL,
                                          CCC_NULL, CCC_NULL, CCC_NULL, CCC_NULL, CCC_NULL,
                                          CCC_NULL, CCC_NULL, CCC_NULL, CCC_NULL, CCC_NULL,
                                          CCC_NULL],
                              @[CCC_NULL, CCC_NULL, CCC_NULL, CCC_NULL, @"3",     CCC_NULL,
                                          @"2",     CCC_NULL, @"1",     CCC_NULL, CCC_NULL,
                                          CCC_NULL, CCC_NULL, CCC_NULL, CCC_NULL, CCC_NULL,
                                         CCC_NULL],
                              @[CCC_NULL, CCC_NULL, CCC_NULL, CCC_NULL, CCC_NULL, CCC_NULL,
                                          CCC_NULL, CCC_NULL, CCC_NULL, @"4",     CCC_NULL,
                                          CCC_NULL, @"3", CCC_NULL,    @"2",      CCC_NULL,
                                          @"1"],
                              @[CCC_NULL, CCC_NULL, CCC_NULL, CCC_NULL, CCC_NULL, CCC_NULL,
                                          CCC_NULL, CCC_NULL, CCC_NULL, CCC_NULL, CCC_NULL,
                                          CCC_NULL, CCC_NULL, CCC_NULL, CCC_NULL, CCC_NULL,
                                          CCC_NULL]
                              ];
    
    // NOTE: This one would also need to change based on the above assumptions
    
    XCTAssert( [result isEqual: expectations],
              "Result:\n%@\nExpected:%@", result, expectations );
    
}

// test the absurd cases too
- (void)testEmpty
{
    NSArray* result = [CrossCommChallenge transform: @[]
                                          withSpans: nil];
    
    NSArray* expectations = @[];
    
    XCTAssert( [result isEqual: expectations],
              "Result:\n%@\nExpected:%@", result, expectations );
    
}

// more absurdity )0 spans)
- (void)testZero
{
    NSArray* result = [CrossCommChallenge transform: @[
                                                       @[@"1",@"2"],
                                                       @[@"2",@"1"],
                                                       @[@"3"]
                                                       ]
                                          withSpans: @{
                                                       @"1": @[@0,@1],
                                                       @"2": @[@0, @0]
                                                       }];
    
    NSArray* expectations = @[
                              @[@"3"]
                              ];
    
    XCTAssert( [result isEqual: expectations],
              "Result:\n%@\nExpected:%@", result, expectations );
    
}

@end
